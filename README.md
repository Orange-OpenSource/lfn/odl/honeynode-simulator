[![pipeline status](https://gitlab.com/Orange-OpenSource/lfn/odl/honeynode-simulator/badges/master/pipeline.svg)](https://gitlab.com/Orange-OpenSource/lfn/odl/honeynode-simulator/-/commits/master)
[![coverage report](https://gitlab.com/Orange-OpenSource/lfn/odl/honeynode-simulator/badges/master/coverage.svg)](https://gitlab.com/Orange-OpenSource/lfn/odl/honeynode-simulator/-/commits/master)

# Honeynode a WDM and OTN device simulator

Honeynode is a simulator for WDM and OTN devices managed by the Netconf protocol.
It is based on the FD.io honeycomb project, an open-source generic Netconf broker.<BR>
https://wiki.fd.io/view/Honeycomb<BR>
Honeynode can be considered as a specialized agent of Honeycomb. <BR>
It currently supports OpenROADM device versions 1.2.1 (device_1_2_1 branch), 2.2.1(device_2_2_1 branch) and 7.1 (master branch)
and partially Openconfig.
* http://openroadm.org/
* http://openconfig.net/

## Installation

You need a working openjdk11 installation to build the project.

Under GNU/Linux, just run *buildHoneynode.sh* at project root folder.
The resulting executable will be located at
*./honeynode-distribution/target/honeynode-distribution-X.Y.Z-hc/honeynode-distribution-X.Y.Z/honeycomb-tpce*

## Usage

honeycomb-tpce port initial-config-xml
Eg : honeycomb-tpce 17832 sample-config-ROADM.xml (rest-http port will be 81 + netconf-port last two digits : 8132)

## License

This project is distributed under the Apache License version 2 (ALv2).
OpenConfig models are also distributed under ALv2.
OpenROADM models license appears in their YANG description field (BSD 3-clause).
FD.io honeycomb poms consume OpenDaylight dependencies under the Eclipse Project License (EPL).
Files under the netconf-impl folder comes from the OpenDaylight project and are licensed under EPL.
