/*
 * Copyright (c) 2022 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;

import com.google.inject.Inject;
import io.fd.honeycomb.transportpce.device.notifications.DeviceNotificationProducer;
import io.fd.honeycomb.transportpce.device.utils.DeviceUtil;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.CurrentPmDescription;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.pm.description.PmGenerationRules;
import org.opendaylight.yang.gen.v1.http.org.openroadm.common.state.types.rev191129.State;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.PortsBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.PortsKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacksKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.group.CurrentPm;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntry;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.types.rev200327.PmDataType;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

public final class CurrentPmChangeListener implements DataTreeChangeListener<CurrentPm> {

    private static final Logger LOG = LoggerFactory.getLogger(CurrentPmChangeListener.class);

    @Inject
    private DeviceUtil deviceUtil;
    @Inject
    private DeviceNotificationProducer deviceNotificationProducer;

    @Override
    public void onDataTreeChanged(Collection<DataTreeModification<CurrentPm>> changes) {
        LOG.info("onDataTreeChanged - CurrentPm");
        for (DataTreeModification<CurrentPm> change : changes) {
            final DataObjectModification<CurrentPm> rootNode = change.getRootNode();
            final DataTreeIdentifier<CurrentPm> rootPath = change.getRootPath();
            switch (rootNode.getModificationType()) {
                case SUBTREE_MODIFIED:
                case WRITE:
                    CurrentPmDescription currentPmDescription = deviceUtil.readContainerFromDataStore(
                            LogicalDatastoreType.OPERATIONAL, InstanceIdentifier.create(CurrentPmDescription.class));
                    if (rootNode.getDataAfter().getMeasurement() != null &&
                            !rootNode.getDataAfter().getMeasurement().isEmpty() &&
                            currentPmDescription != null) {
                        LinkedList<InstanceIdentifier.PathArgument> path = new LinkedList<>((Collection<? extends
                                InstanceIdentifier.PathArgument>) rootPath.getRootIdentifier().getPathArguments());
                        path.removeLast();
                        InstanceIdentifier<CurrentPmEntry> iidCurrentPmEntry =
                                (InstanceIdentifier<CurrentPmEntry>) InstanceIdentifier.create(path);
                        Interface theInterface = (Interface) deviceUtil.readContainerFromDataStore(
                                LogicalDatastoreType.CONFIGURATION,
                                InstanceIdentifier.keyOf(iidCurrentPmEntry).getPmResourceInstance());
                        // Get the PmGenerationRules that may administer the CurrentPm
                        PmGenerationRules targetPmGenerationRules = currentPmDescription.getPmGenerationRules().stream()
                                .filter(pmGenerationRules ->
                                        theInterface.getType().equals(pmGenerationRules.getTriggeringInterface()) &&
                                                checkIfSamePmType(rootNode.getDataAfter(), pmGenerationRules))
                                .findFirst()
                                .orElse(null);
                        if (targetPmGenerationRules == null) {
                            continue;
                        }
                        InstanceIdentifier<Ports> iiPort = InstanceIdentifier
                                .create(OrgOpenroadmDevice.class)
                                .child(CircuitPacks.class, new CircuitPacksKey(
                                        theInterface.getSupportingCircuitPackName()))
                                .child(Ports.class, new PortsKey(
                                        (String) theInterface.getSupportingPort()));
                        Ports port = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, iiPort);
                        // Check if the measurement is out of the boundaries defined by the PmGenerationRules
                        boolean havePmOutOfBounds = rootNode.getDataAfter().getMeasurement().stream()
                                .anyMatch(pmValue -> checkIfPmValueOutOfBounds(pmValue.getPmParameterValue(),
                                        targetPmGenerationRules));
                        if (havePmOutOfBounds && port.getOperationalState().equals(State.InService)) {
                            LOG.warn("Detected abnormal {} PM parameter value !", rootNode.getDataAfter().getType());
                            changeOperationalStatePort(port, iiPort, State.OutOfService);
                        } else if (port.getOperationalState().equals(State.OutOfService)
                                && !havePmOutOfBounds
                                && rootNode.getDataBefore() != null
                                && !rootNode.getDataBefore().getMeasurement().isEmpty()
                                && rootNode.getDataBefore().getMeasurement().stream()
                                .anyMatch(pmValue -> checkIfPmValueOutOfBounds(pmValue.getPmParameterValue(),
                                        targetPmGenerationRules))) {
                            changeOperationalStatePort(port, iiPort, State.InService);
                        }
                    }
                    break;
                case DELETE:
                default:
                    break;
            }
        }
    }

    private boolean checkIfSamePmType(CurrentPm currentPm, PmGenerationRules pmGenerationRules) {
        return currentPm.getType().getName().equalsIgnoreCase(pmGenerationRules.getPmNamePrefix());
    }

    private boolean checkIfPmValueOutOfBounds(PmDataType pmDataType, PmGenerationRules pmGenerationRules) {
        BigDecimal pmValue = new BigDecimal(pmDataType.stringValue());
        return pmValue.compareTo(pmGenerationRules.getMaxValue()) > 0 ||
                pmValue.compareTo(pmGenerationRules.getMinValue()) < 0;
    }

    private void changeOperationalStatePort(Ports port, InstanceIdentifier<Ports> iiPort, State newState) {
        deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, iiPort,
                new PortsBuilder(port).setOperationalState(newState).build());
        LOG.info("Changing the operational state of the port {} to {}", port.getPortName(), newState.getName());
        deviceNotificationProducer.sendDeviceChangeNotification(iiPort);
    }
}
