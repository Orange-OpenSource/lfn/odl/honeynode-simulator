/*
 * Copyright (c) 2021 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.PortsBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.PortsKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacksKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.interfaces.grp.InterfaceBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.port.Interfaces;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.port.InterfacesBuilder;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import io.fd.honeycomb.transportpce.device.utils.DeviceUtil;

/**
 * @author Gilles Thouenon - Orange
 */
public final class DeviceInterfaceChangeListener implements DataTreeChangeListener<Interface> {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceInterfaceChangeListener.class);
    @Inject
    private DeviceUtil deviceUtil;

    @Override
    public void onDataTreeChanged(Collection<DataTreeModification<Interface>> changes) {
        LOG.info("onDataTreeChanged - Interface");
        for (DataTreeModification<Interface> change : changes) {
            final DataObjectModification<Interface> rootNode = change.getRootNode();
            final DataTreeIdentifier<Interface> rootPath = change.getRootPath();

            if (rootNode != null) {
                LOG.debug("Received Interface change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                    rootNode.getModificationType(), rootNode.getDataType(), rootNode.getDataBefore(),
                    rootNode.getDataAfter(), rootNode.getIdentifier());
                InstanceIdentifier<Interface> interfaceId = null;
                if (rootPath.getRootIdentifier().getTargetType().getName().equals(Interface.class.getName())) {
                    interfaceId = (InstanceIdentifier<Interface>) InstanceIdentifier
                        .create(rootPath.getRootIdentifier().getPathArguments());
                }
                switch (rootNode.getModificationType()) {
                    case SUBTREE_MODIFIED:
                    case WRITE:
                        LOG.info("Interface is being created or modified");
                        updateInterfaceOnOperationalDS(rootNode.getDataBefore(), rootNode.getDataAfter(), interfaceId);
                        break;
                    case DELETE:
                        LOG.info("Interface is being deleted");
                        deviceUtil.deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, interfaceId);
                        updateCpPortInterfaceList(rootNode.getDataBefore(), true);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * update the circuit-pack in the operational datastore after a change received
     * on the config datastore.
     *
     * @param cpBefore
     *            initial configuration of the circuit-pack
     * @param cpAfter
     *            new configuration of the circuit-pack
     * @param id
     *            Instance Identifier of the circuit-pack
     */
    private void updateInterfaceOnOperationalDS(Interface interfaceBefore, Interface interfaceAfter,
        InstanceIdentifier<Interface> id) {
        if (id == null) {
            LOG.warn("Error to create or update interface in operational DS - interface instance id is null");
            return;
        }
        Interface oldInterface = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, id);
        InterfaceBuilder interfaceBldr;
        if (oldInterface != null) {
            interfaceBldr = new InterfaceBuilder(oldInterface);
            if ((interfaceBefore.getAdministrativeState() == null && interfaceAfter.getAdministrativeState() != null)
                || (!interfaceBefore.getAdministrativeState().equals(interfaceAfter.getAdministrativeState()))) {
                LOG.info("administrative state of interface {} changed", interfaceBefore.getName());
                interfaceBldr.setAdministrativeState(interfaceAfter.getAdministrativeState())
                    .setOperationalState(
                        deviceUtil.setOperationalStateFromAdminState(interfaceAfter.getAdministrativeState()));
            }

        } else {
            LOG.info("creating new interface {} in operational DS", interfaceAfter.getName());
            interfaceBldr = new InterfaceBuilder(interfaceAfter)
                .setOperationalState(
                    deviceUtil.setOperationalStateFromAdminState(interfaceAfter.getAdministrativeState()));
            updateCpPortInterfaceList(interfaceAfter, false);
        }
        deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, id, interfaceBldr.build());
    }

    private void updateCpPortInterfaceList(Interface interf, boolean isDeleting) {
        InstanceIdentifier<Ports> supportedPortId = InstanceIdentifier.create(OrgOpenroadmDevice.class)
            .child(CircuitPacks.class, new CircuitPacksKey(interf.getSupportingCircuitPackName()))
            .child(Ports.class, new PortsKey(interf.getSupportingPort().toString()));
        Ports supportedPort = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, supportedPortId);
        if (supportedPort == null) {
            LOG.warn("Error updating supported port with interface name - port does not exist in device configuration");
            return;
        }
        List<Interfaces> supportInterList = supportedPort.getInterfaces();
        if ((supportInterList == null || supportInterList.isEmpty()) && isDeleting) {
            LOG.warn("Error deleting interface {} from port {} of circuit-pack {}. Interface does not exist.",
                interf.getName(),
                interf.getSupportingPort(), interf.getSupportingCircuitPackName());
            return;
        }
        if ((supportInterList == null || supportInterList.isEmpty()) && !isDeleting) {
            supportInterList = new ArrayList<>();
        }
        Interfaces portInterfaces = new InterfacesBuilder().setInterfaceName(interf.getName()).build();
        if (!supportInterList.contains(portInterfaces) && !isDeleting) {
            LOG.info("updating circuit-pack {} port {} interface list with {}", interf.getSupportingCircuitPackName(),
                interf.getSupportingPort(), interf.getName());
            supportInterList.add(portInterfaces);
        }
        if (supportInterList.contains(portInterfaces) && isDeleting) {
            LOG.info("deleting from circuit-pack {} port {} interface {}", interf.getSupportingCircuitPackName(),
                interf.getSupportingPort(), interf.getName());
            supportInterList.remove(portInterfaces);
        }
        Ports portAfter = new PortsBuilder(supportedPort).setInterfaces(supportInterList).build();
        deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, supportedPortId, portAfter);
    }
}