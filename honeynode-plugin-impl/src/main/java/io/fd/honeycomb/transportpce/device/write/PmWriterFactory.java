/*
 * Copyright (c) 2022 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import io.fd.honeycomb.translate.util.write.BindingBrokerWriter;
import io.fd.honeycomb.translate.write.WriterFactory;
import io.fd.honeycomb.translate.write.registry.ModifiableWriterRegistryBuilder;
import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmList;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.group.CurrentPm;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntry;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import javax.annotation.Nonnull;

public class PmWriterFactory implements WriterFactory {
    private static final InstanceIdentifier<CurrentPm> CURRENT_PM_CONTAINER_ID = InstanceIdentifier
            .create(CurrentPmList.class).child(CurrentPmEntry.class).child(CurrentPm.class);

    @Inject
    @Named("device-databroker")
    private DataBroker deviceDataBroker;

    @Inject
    private CurrentPmChangeListener cpcl;

    @Override
    public void init(@Nonnull final ModifiableWriterRegistryBuilder registry) {
        registry.wildcardedSubtreeAdd(new BindingBrokerWriter<>(CURRENT_PM_CONTAINER_ID, deviceDataBroker));
        deviceDataBroker.registerDataTreeChangeListener(DataTreeIdentifier.create(LogicalDatastoreType.OPERATIONAL,
                CURRENT_PM_CONTAINER_ID), cpcl);
    }
}
