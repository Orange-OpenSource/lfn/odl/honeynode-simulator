/*
 * Copyright (c) 2018 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.read;

import java.util.concurrent.ExecutionException;

import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.WriteTransaction;
import org.opendaylight.mdsal.common.api.CommitInfo;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.CurrentPmDescription;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.CurrentPmDescriptionBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmList;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmListBuilder;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.Futures;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import io.fd.honeycomb.translate.read.ReaderFactory;
import io.fd.honeycomb.translate.read.registry.ModifiableReaderRegistryBuilder;
import io.fd.honeycomb.translate.util.read.BindingBrokerReader;
import io.fd.honeycomb.transportpce.device.configuration.PmConfiguration;

/**
 * @author Martial COULIBALY ( mcoulibaly.ext@orange.com ) on behalf of Orange
 */
public class PmReaderFactory implements ReaderFactory {

    private static final Logger LOG = LoggerFactory.getLogger(PmReaderFactory.class);
    public static final InstanceIdentifier<CurrentPmList> PM_CONTAINER_ID =
            InstanceIdentifier.create(CurrentPmList.class);
    public static final InstanceIdentifier<CurrentPmDescription> PM_DESCRIPTION_CONTAINER_ID =
            InstanceIdentifier.create(CurrentPmDescription.class);

    @Inject
    @Named("device-databroker")
    private DataBroker dataBroker;

    @Inject
    private PmConfiguration pmConfiguration;


    @Override
    public void init(final ModifiableReaderRegistryBuilder registry) {
        registry.add(new BindingBrokerReader<>(PM_CONTAINER_ID, dataBroker, LogicalDatastoreType.OPERATIONAL,
                CurrentPmListBuilder.class));
        registry.add(new BindingBrokerReader<>(PM_DESCRIPTION_CONTAINER_ID, dataBroker,
                LogicalDatastoreType.OPERATIONAL, CurrentPmDescriptionBuilder.class));
        writeXMLDataToOper();
    }

    /**
     * Write xml data from {@link PmConfiguration} (currentPmList, CurrentPmDescription)
     * to operational data.
     */
    public void writeXMLDataToOper() {
        LOG.info("Writing xml file pm data to oper datastore");
        CurrentPmList pmList = this.pmConfiguration.getCurrentPmList();
        if (pmList != null && !pmList.getCurrentPmEntry().isEmpty()) {
            LOG.info("Getting pm info from xml file for device ");
            CurrentPmListBuilder result = new CurrentPmListBuilder(pmList);
            InstanceIdentifier<CurrentPmList> iid = InstanceIdentifier.create(CurrentPmList.class);
            WriteTransaction writeTx = this.dataBroker.newWriteOnlyTransaction();
            LOG.info("WriteTransaction is ok, copy currentPmList to oper datastore");
            writeTx.put(LogicalDatastoreType.OPERATIONAL, iid, result.build());
            FluentFuture<? extends @NonNull CommitInfo> future = writeTx.commit();
            try {
                Futures.getChecked(future, ExecutionException.class);
                LOG.info("CurrentPmList was written to oper datastore");
            } catch (ExecutionException e) {
                LOG.error("Failed to write currentPmList to oper datastore");
            }
        } else {
            LOG.error("currentPmList data operation gets from xml file is null !");
        }
        CurrentPmDescription pmDescription = this.pmConfiguration.getPmDescription();
        if (pmDescription != null && !pmDescription.getPmGenerationRules().isEmpty()) {
            LOG.info("Getting pm description from xml file for device ");
            CurrentPmDescriptionBuilder result = new CurrentPmDescriptionBuilder(pmDescription);
            InstanceIdentifier<CurrentPmDescription> iid = InstanceIdentifier.create(CurrentPmDescription.class);
            WriteTransaction writeTx = this.dataBroker.newWriteOnlyTransaction();
            LOG.info("WriteTransaction is ok, copy currentPmDescription to oper datastore");
            writeTx.put(LogicalDatastoreType.OPERATIONAL, iid, result.build());
            FluentFuture<? extends @NonNull CommitInfo> future = writeTx.commit();
            try {
                Futures.getChecked(future, ExecutionException.class);
                LOG.info("CurrentPmDescription was written to oper datastore");
            } catch (ExecutionException e) {
                LOG.error("Failed to write currentPmDescription to oper datastore");
            }
        } else {
            LOG.error("CurrentPmDescription data operation gets from xml file is null !");
        }
    }
}
