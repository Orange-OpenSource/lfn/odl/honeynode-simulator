/*
 * Copyright (c) 2021 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;


import com.google.inject.Inject;
import io.fd.honeycomb.transportpce.device.notifications.DeviceNotificationProducer;
import io.fd.honeycomb.transportpce.device.utils.DeviceUtil;
import java.util.Collection;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.PortsBuilder;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Gilles Thouenon - Orange
 */
public final class DevicePortChangeListener implements DataTreeChangeListener<Ports> {

    private static final Logger LOG = LoggerFactory.getLogger(DevicePortChangeListener.class);

    @Inject
    private DeviceUtil deviceUtil;
    @Inject
    private DeviceNotificationProducer deviceNotificationProducer;

    @Override
    public void onDataTreeChanged(Collection<DataTreeModification<Ports>> changes) {
        LOG.info("onDataTreeChanged - Ports");
        for (DataTreeModification<Ports> change : changes) {
            final DataObjectModification<Ports> rootNode = change.getRootNode();
            final DataTreeIdentifier<Ports> rootPath = change.getRootPath();
            if (rootNode != null && rootNode.getDataBefore() != null
                && !rootNode.getDataBefore().equals(rootNode.getDataAfter())) {
                LOG.debug("Received Ports change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                    rootNode.getModificationType(), rootNode.getDataType(), rootNode.getDataBefore(),
                    rootNode.getDataAfter(), rootNode.getIdentifier());
                InstanceIdentifier<Ports> portId = null;
                if (rootPath.getRootIdentifier().getTargetType().getName().equals(Ports.class.getName())) {
                    portId = (InstanceIdentifier<Ports>) InstanceIdentifier
                            .create(rootPath.getRootIdentifier().getPathArguments());
                }
                switch (rootNode.getModificationType()) {
                    case SUBTREE_MODIFIED:
                    case WRITE:
                        LOG.info("port is being modified");
                        updatePortOnOperationalDS(rootNode.getDataBefore(), rootNode.getDataAfter(), portId);
                        deviceNotificationProducer.sendDeviceChangeNotification(portId);
                        break;
                    case DELETE:
                        LOG.info("port is being deleted");
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * update the port in the operational datastore after a change received on the
     * config datastore.
     *
     * @param portBefore
     *            initial configuration of the port
     * @param portAfter
     *            new configuration of the port
     * @param id
     *            Instance Identifier of the port
     */
    private synchronized void updatePortOnOperationalDS(Ports portBefore, Ports portAfter,
        InstanceIdentifier<Ports> id) {
        if (id == null) {
            LOG.warn("Error to update port in operational DS - port instance id is null");
            return;
        }
        Ports oldPort = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, id);
        PortsBuilder portBldr = new PortsBuilder(oldPort);
        if (!portBefore.getAdministrativeState().equals(portAfter.getAdministrativeState())) {
            LOG.info("administrative state of port {} changed", portBefore.getPortName());
            portBldr.setAdministrativeState(portAfter.getAdministrativeState())
                .setOperationalState(deviceUtil.setOperationalStateFromAdminState(portAfter.getAdministrativeState()));
        }
        deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, id, portBldr.build());
    }
}
