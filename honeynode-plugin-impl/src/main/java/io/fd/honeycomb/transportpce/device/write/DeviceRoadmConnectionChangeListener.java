/*
 * Copyright (c) 2021 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;

import java.util.Collection;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.RoadmConnections;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import io.fd.honeycomb.transportpce.device.utils.DeviceUtil;

/**
 * @author Gilles Thouenon - Orange
 */
public final class DeviceRoadmConnectionChangeListener implements DataTreeChangeListener<RoadmConnections> {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceRoadmConnectionChangeListener.class);
    @Inject
    private DeviceUtil deviceUtil;

    @Override
    public void onDataTreeChanged(Collection<DataTreeModification<RoadmConnections>> changes) {
        LOG.info("onDataTreeChanged - RoadmConnections");
        for (DataTreeModification<RoadmConnections> change : changes) {
            final DataObjectModification<RoadmConnections> rootNode = change.getRootNode();
            final DataTreeIdentifier<RoadmConnections> rootPath = change.getRootPath();
            LOG.debug("Received RoadmConnections change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                rootNode.getModificationType(), rootNode.getDataType(), rootNode.getDataBefore(),
                rootNode.getDataAfter(), rootNode.getIdentifier());
            InstanceIdentifier<RoadmConnections> connectionId = null;
            if (rootPath.getRootIdentifier().getTargetType().getName().equals(RoadmConnections.class.getName())) {
                connectionId = (InstanceIdentifier<RoadmConnections>) InstanceIdentifier
                    .create(rootPath.getRootIdentifier().getPathArguments());
            }
            switch (rootNode.getModificationType()) {
                case SUBTREE_MODIFIED:
                case WRITE:
                    LOG.info("RoadmConnections is being created or modified");
                    copyConnectionOnOperationalDS(rootNode.getDataAfter(), connectionId);
                    break;
                case DELETE:
                    LOG.info("RoadmConnections is being deleted");
                    deviceUtil.deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, connectionId);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * copy the connection in the operational datastore after a change received on
     * the config datastore.
     *
     * @param connectionAfter
     *            new configuration of the circuit-pack
     * @param id
     *            Instance Identifier of the circuit-pack
     */
    private void copyConnectionOnOperationalDS(RoadmConnections connectionAfter,
        InstanceIdentifier<RoadmConnections> id) {
        if (id == null) {
            LOG.warn("Error to copy connection in operational DS - connection instance id is null");
            return;
        }
        RoadmConnections connection = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, id);
        if (connection != null) {
            LOG.info("Updating existing connection {} in operational DS", connectionAfter.getConnectionName());
        } else {
            LOG.info("creating new connection {} in operational DS", connectionAfter.getConnectionName());
        }
        deviceUtil.overwriteContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, id, connectionAfter);
    }

}