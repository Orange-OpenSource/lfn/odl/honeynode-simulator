/*
 * Copyright (c) 2022 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.rpcs;

import com.google.inject.Inject;
import io.fd.honeycomb.rpc.RpcService;
import io.fd.honeycomb.transportpce.device.utils.DeviceUtil;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.HoneynodeRpcActions;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.PmInteractInput;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.PmInteractOutput;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.PmInteractOutputBuilder;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.PmToBeSetOrCreated;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.PmToGetClearOrDelete;
import org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021.pm.interact.output.RetrievedPmResultsBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.common.types.rev200529.RpcStatus;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmList;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.CurrentPmListBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.group.CurrentPm;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.group.CurrentPmKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntry;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntryBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.list.CurrentPmEntryKey;
import org.opendaylight.yang.gen.v1.http.org.openroadm.pm.rev200529.current.pm.val.group.Measurement;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.common.QName;
import org.opendaylight.yangtools.yang.model.api.SchemaPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class PmInteractService implements RpcService<PmInteractInput, PmInteractOutput> {
    private static final Logger LOG = LoggerFactory.getLogger(PmInteractService.class);
    private static final String localName = "pm-interact";
    private static final QName name = QName.create(PmInteractInput.QNAME, localName);
    private static final SchemaPath schemaPath = SchemaPath.ROOT.createChild(name);

    private static final String EMPTY_CURRENT_PM_LIST_MSG = "There is no CurrentPmList in DataStore";
    private static final String EMPTY_INTERFACE_MSG = "The resource instance doesn't exist";
    private static final String INVALID_CURRENT_PM_ENTRY_MSG = "The inputs of CurrentPmEntry are incorrect";
    private static final String INVALID_CURRENT_PM_MSG = "The inputs of CurrentPm are incorrect";
    private static final String INVALID_MEASUREMENT = "The inputs of Measurement are incorrect";
    private static final String INVALID_PM_GET_CLEAR_OR_DELETE = "The input pm-to-get-clear-or-delete is null";
    private static final String INVALID_PM_BE_SET_OR_CREATED = "The input pm-to-be-set-or-created is null";

    // Contains the CurrentPm that have been _SET
    public static Map<InstanceIdentifier<CurrentPm>, CurrentPm> setCurrentPmMap = new HashMap<>();

    @Inject
    private DeviceUtil deviceUtil;

    @Override
    public SchemaPath getManagedNode() {
        return schemaPath;
    }

    @Nonnull
    @Override
    public CompletionStage<PmInteractOutput> invoke(@Nullable PmInteractInput pmInteractInput) {
        LOG.info("RPC PmInteract request received !");
        PmInteractOutputBuilder output = new PmInteractOutputBuilder();
        CompletableFuture<PmInteractOutput> result = new CompletableFuture<>();
        // Compliance checks
        if (!validateInput(pmInteractInput, output)) {
            result.complete(output.build());
            return result;
        }
        LOG.info("rpc-action _{}", pmInteractInput.getRpcAction());

        List<InstanceIdentifier<CurrentPm>> iidCurrentPmArray = new ArrayList<>();
        InstanceIdentifier<CurrentPmList> iidCurrentPmList = InstanceIdentifier.create(CurrentPmList.class);
        List<CurrentPmEntry> newCurrentPmEntry = new ArrayList<>();
        CurrentPmListBuilder newCurrentPmList = new CurrentPmListBuilder();
        CurrentPmList currentPmListDS;
        CurrentPmEntry currentPmEntryDS;

        switch (pmInteractInput.getRpcAction()) {
            case Get:
                currentPmListDS = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                List<org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021
                        .pm.interact.output.retrieved.pm.results.CurrentPmEntry> currentPmEntries = new ArrayList<>();
                if (currentPmListDS != null) {
                    if (pmInteractInput.getPmToGetClearOrDelete() == null ||
                            pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry() == null ||
                            pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry().isEmpty()) {
                        // Get all the CurrentPmEntry
                        for (CurrentPmEntry currentPmEntryElem : currentPmListDS.getCurrentPmEntry())
                            currentPmEntries.add(
                                    new org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling
                                            .rev211021.pm.interact.output.retrieved.pm.results
                                            .CurrentPmEntryBuilder(currentPmEntryElem).build());
                    } else {
                        // Get only the CurrentPmEntry specified in the input
                        for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021
                                .pm.interact.input.pm.to.get.clear.or.delete.CurrentPmEntry currentPmEntryElem :
                                pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry()) {
                            currentPmEntryDS = getCurrentPmEntry(currentPmListDS.getCurrentPmEntry(),
                                    currentPmEntryElem);
                            if (currentPmEntryDS == null) {
                                LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                                output.setStatus(RpcStatus.Failed)
                                        .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                                result.complete(output.build());
                                return result;
                            }
                            currentPmEntries.add(new org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling
                                    .rev211021.pm.interact.output.retrieved.pm.results
                                    .CurrentPmEntryBuilder(currentPmEntryDS).build());
                        }
                    }
                } else {
                    output.setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                }
                LOG.info("The PMs has been successfully retrieved !");
                output.setRetrievedPmResults(
                        new RetrievedPmResultsBuilder().setCurrentPmEntry(currentPmEntries).build());
                break;

            case Set:
                HashMap<InstanceIdentifier<CurrentPm>, CurrentPm> tempSetCurrentPmMap = new HashMap<>();
                currentPmListDS = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                if (currentPmListDS == null) {
                    LOG.warn(EMPTY_CURRENT_PM_LIST_MSG);
                    output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                    result.complete(output.build());
                    return result;
                }
                for (org.opendaylight.yang.gen.v1
                        .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.be.set.or.created
                        .CurrentPmEntry currentPmEntryElem :
                        pmInteractInput.getPmToBeSetOrCreated().getCurrentPmEntry()) {
                    if (deviceUtil.readContainerFromDataStore(LogicalDatastoreType.CONFIGURATION,
                            currentPmEntryElem.getPmResourceInstance()) == null) {
                        LOG.warn(EMPTY_INTERFACE_MSG);
                        output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(EMPTY_INTERFACE_MSG);
                        result.complete(output.build());
                        return result;
                    }
                    currentPmEntryDS = getCurrentPmEntry(currentPmListDS.getCurrentPmEntry(), currentPmEntryElem);
                    if (currentPmEntryDS == null) {
                        LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                        output.setStatus(RpcStatus.Failed)
                                .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                        result.complete(output.build());
                        return result;
                    }
                    for (CurrentPm currentPmElem : currentPmEntryElem.getCurrentPm()) {
                        CurrentPm currentPmDS = getCurrentPm(currentPmEntryDS.getCurrentPm(), currentPmElem);
                        if (currentPmDS == null) {
                            LOG.warn("CurrentPm {} doesn't exist", currentPmElem);
                            output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm doesn't exist");
                            result.complete(output.build());
                            return result;
                        }
                        InstanceIdentifier<CurrentPm> iidCurrentPm = iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                        currentPmEntryElem.getPmResourceInstance(),
                                        currentPmEntryElem.getPmResourceType(),
                                        currentPmEntryElem.getPmResourceTypeExtension()))
                                .child(CurrentPm.class, new CurrentPmKey(
                                        currentPmElem.getDirection(),
                                        currentPmElem.getExtension(),
                                        currentPmElem.getLocation(),
                                        currentPmElem.getType()));
                        if (setCurrentPmMap.containsKey(iidCurrentPm)) {
                            LOG.warn("CurrentPm {} has been already set", currentPmElem);
                            output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm has been already set");
                            result.complete(output.build());
                            return result;
                        }
                        tempSetCurrentPmMap.put(iidCurrentPm, currentPmDS);
                    }
                    newCurrentPmEntry.add(new CurrentPmEntryBuilder(currentPmEntryElem).setStartTime(null).build());
                }
                // Merge the Maps
                setCurrentPmMap.putAll(tempSetCurrentPmMap);

                newCurrentPmList.setCurrentPmEntry(newCurrentPmEntry);
                deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList, newCurrentPmList.build());
                LOG.info("The PMs has been successfully set !");
                output.setStatusMessage("The PMs has been successfully set !");
                break;

            case Create:
                for (org.opendaylight.yang.gen.v1
                        .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.be.set.or.created
                        .CurrentPmEntry currentPmEntryElem :
                        pmInteractInput.getPmToBeSetOrCreated().getCurrentPmEntry()) {
                    if (deviceUtil.readContainerFromDataStore(LogicalDatastoreType.CONFIGURATION,
                            currentPmEntryElem.getPmResourceInstance()) == null) {
                        LOG.warn(EMPTY_INTERFACE_MSG);
                        output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(EMPTY_INTERFACE_MSG);
                        result.complete(output.build());
                        return result;
                    }
                    for (CurrentPm currentPmElem : currentPmEntryElem.getCurrentPm()) {
                        InstanceIdentifier<CurrentPm> iidCurrentPm = iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                        currentPmEntryElem.getPmResourceInstance(),
                                        currentPmEntryElem.getPmResourceType(),
                                        currentPmEntryElem.getPmResourceTypeExtension()))
                                .child(CurrentPm.class, new CurrentPmKey(
                                        currentPmElem.getDirection(),
                                        currentPmElem.getExtension(),
                                        currentPmElem.getLocation(),
                                        currentPmElem.getType()));
                        if (setCurrentPmMap.containsKey(iidCurrentPm)) {
                            LOG.warn("CurrentPm {} already exist and is actually set", currentPmElem);
                            output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm already exist and is actually set");
                            result.complete(output.build());
                            return result;
                        }
                    }
                    newCurrentPmEntry.add(new CurrentPmEntryBuilder(currentPmEntryElem).build());
                }
                deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList, newCurrentPmList.setCurrentPmEntry(newCurrentPmEntry).build());
                LOG.info("The PMs has been successfully created !");
                output.setStatusMessage("The PMs has been successfully created !");
                break;

            case Delete:
                List<InstanceIdentifier<CurrentPmEntry>> iidCurrentPmEntryList = new ArrayList<>();
                currentPmListDS = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                if (currentPmListDS == null) {
                    LOG.warn(EMPTY_CURRENT_PM_LIST_MSG);
                    output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                    result.complete(output.build());
                    return result;
                }
                for (org.opendaylight.yang.gen.v1
                        .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.get.clear.or.delete
                        .CurrentPmEntry currentPmEntryElem :
                        pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry()) {
                    // Check that input currentPmEntry exist in the datastore
                    currentPmEntryDS = getCurrentPmEntry(currentPmListDS.getCurrentPmEntry(), currentPmEntryElem);
                    if (currentPmEntryDS == null) {
                        LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                        output.setStatus(RpcStatus.Failed)
                                .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                        result.complete(output.build());
                        return result;
                    }
                    // Check if the input requests to delete specific CurrentPm
                    if (currentPmEntryElem.getCurrentPm() != null) {
                        for (org.opendaylight.yang.gen.v1
                                .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.get.clear.or
                                .delete.current.pm.entry.CurrentPm
                                currentPmElem : currentPmEntryElem.getCurrentPm()) {
                            // Check that the CurrentPm exist in the datastore
                            if (!checkCurrentPmDoesExist(currentPmEntryDS.getCurrentPm(), currentPmElem)) {
                                LOG.warn("CurrentPm {} doesn't exist", currentPmElem);
                                output.setStatus(RpcStatus.Failed)
                                        .setStatusMessage("At least one CurrentPm doesn't exist");
                                result.complete(output.build());
                                return result;
                            }
                            iidCurrentPmArray.add(iidCurrentPmList
                                    .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                            currentPmEntryElem.getPmResourceInstance(),
                                            currentPmEntryElem.getPmResourceType(),
                                            currentPmEntryElem.getPmResourceTypeExtension()))
                                    .child(CurrentPm.class, new CurrentPmKey(
                                            currentPmElem.getDirection(),
                                            currentPmElem.getExtension(),
                                            currentPmElem.getLocation(),
                                            currentPmElem.getType())));
                        }
                    }
                    // Check if we have to delete CurrentPmEntry
                    if (currentPmEntryElem.getCurrentPm() == null || currentPmEntryElem.getCurrentPm().isEmpty() ||
                            currentPmEntryElem.getCurrentPm().size() == currentPmEntryDS.getCurrentPm().size())
                        iidCurrentPmEntryList.add(iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                        currentPmEntryElem.getPmResourceInstance(),
                                        currentPmEntryElem.getPmResourceType(),
                                        currentPmEntryElem.getPmResourceTypeExtension())));
                }
                for (InstanceIdentifier<CurrentPm> iidCurrentPm : iidCurrentPmArray) {
                    deviceUtil.deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, iidCurrentPm);
                    setCurrentPmMap.remove(iidCurrentPm);
                }
                for (InstanceIdentifier<CurrentPmEntry> iidCurrentPmEntry : iidCurrentPmEntryList)
                    deviceUtil.deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, iidCurrentPmEntry);
                LOG.info("The PMs has been successfully deleted !");
                output.setStatusMessage("The PMs has been successfully deleted !");
                break;

            case Clear:
                currentPmListDS = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL,
                        iidCurrentPmList);
                if (currentPmListDS == null) {
                    LOG.warn(EMPTY_CURRENT_PM_LIST_MSG);
                    output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(EMPTY_CURRENT_PM_LIST_MSG);
                    result.complete(output.build());
                    return result;
                }
                for (org.opendaylight.yang.gen.v1
                        .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.get.clear.or.delete
                        .CurrentPmEntry currentPmEntryElem :
                        pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry()) {
                    currentPmEntryDS = getCurrentPmEntry(currentPmListDS.getCurrentPmEntry(), currentPmEntryElem);
                    if (currentPmEntryDS == null) {
                        LOG.warn("CurrentPmEntry {} doesn't exist", currentPmEntryElem);
                        output.setStatus(RpcStatus.Failed)
                                .setStatusMessage("At least one CurrentPmEntry doesn't exist");
                        result.complete(output.build());
                        return result;
                    }
                    for (org.opendaylight.yang.gen.v1.http.honeynode.simulator.pm.handling.rev211021
                            .pm.interact.input.pm.to.get.clear.or.delete.current.pm.entry.CurrentPm currentPmElem
                            : currentPmEntryElem.getCurrentPm()) {
                        InstanceIdentifier<CurrentPm> iidCurrentPm = iidCurrentPmList
                                .child(CurrentPmEntry.class, new CurrentPmEntryKey(
                                        currentPmEntryElem.getPmResourceInstance(),
                                        currentPmEntryElem.getPmResourceType(),
                                        currentPmEntryElem.getPmResourceTypeExtension()))
                                .child(CurrentPm.class, new CurrentPmKey(
                                        currentPmElem.getDirection(),
                                        currentPmElem.getExtension(),
                                        currentPmElem.getLocation(),
                                        currentPmElem.getType()));
                        if (!checkCurrentPmDoesExist(currentPmEntryDS.getCurrentPm(), currentPmElem) ||
                                !setCurrentPmMap.containsKey(iidCurrentPm)) {
                            LOG.warn("CurrentPm {} doesn't exist or has never been set", currentPmElem);
                            output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage("At least one CurrentPm doesn't exist or " +
                                            "has never been set");
                            result.complete(output.build());
                            return result;
                        }
                        iidCurrentPmArray.add(iidCurrentPm);
                    }
                }
                for (InstanceIdentifier<CurrentPm> iidCurrentPm : iidCurrentPmArray) {
                    deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, iidCurrentPm,
                            setCurrentPmMap.get(iidCurrentPm));
                    setCurrentPmMap.remove(iidCurrentPm);
                }
                LOG.info("The PMs has been successfully released !");
                output.setStatusMessage("The PMs has been successfully released !");
                break;
        }
        output.setStatus(RpcStatus.Successful);
        result.complete(output.build());
        return result;
    }

    private Boolean checkCurrentPmDoesExist(List<CurrentPm> currentPmList, org.opendaylight.yang.gen.v1.http
            .honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.get.clear.or.delete.current.pm.entry
            .CurrentPm currentPmToCheck) {
        return currentPmList.stream().anyMatch(currentPm ->
                currentPm.getType().equals(currentPmToCheck.getType()) &&
                        currentPm.getExtension().equals(currentPmToCheck.getExtension()) &&
                        currentPm.getLocation().equals(currentPmToCheck.getLocation()) &&
                        currentPm.getDirection().equals(currentPmToCheck.getDirection()));
    }

    private CurrentPmEntry getCurrentPmEntry(List<CurrentPmEntry> currentPmEntryList, org.opendaylight.yang.gen.v1
            .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.get.clear.or.delete.CurrentPmEntry
            currentPmEntryToGet) {
        return currentPmEntryList.stream()
                .filter(currentPmEntryElem -> currentPmEntryElem.getPmResourceInstance()
                        .equals(currentPmEntryToGet.getPmResourceInstance()) && currentPmEntryElem.getPmResourceType()
                        .equals(currentPmEntryToGet.getPmResourceType()) && currentPmEntryElem
                        .getPmResourceTypeExtension().equals(currentPmEntryToGet.getPmResourceTypeExtension()))
                .findFirst()
                .orElse(null);
    }

    private CurrentPmEntry getCurrentPmEntry(List<CurrentPmEntry> currentPmEntryList, org.opendaylight.yang.gen.v1
            .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.be.set.or.created.CurrentPmEntry
            currentPmEntryToGet) {
        return currentPmEntryList.stream()
                .filter(currentPmEntryElem -> currentPmEntryElem.getPmResourceInstance()
                        .equals(currentPmEntryToGet.getPmResourceInstance()) && currentPmEntryElem.getPmResourceType()
                        .equals(currentPmEntryToGet.getPmResourceType()) && currentPmEntryElem
                        .getPmResourceTypeExtension().equals(currentPmEntryToGet.getPmResourceTypeExtension()))
                .findFirst()
                .orElse(null);
    }

    private CurrentPm getCurrentPm(List<CurrentPm> currentPmEntryList, CurrentPm currentPmToGet) {
        return currentPmEntryList.stream()
                .filter(currentPmElem -> currentPmElem.getType()
                        .equals(currentPmToGet.getType()) && currentPmElem.getExtension()
                        .equals(currentPmToGet.getExtension()) && currentPmElem
                        .getLocation().equals(currentPmToGet.getLocation()) &&
                        currentPmElem.getDirection().equals(currentPmToGet.getDirection()))
                .findFirst()
                .orElse(null);
    }

    private Boolean validateInput(PmInteractInput pmInteractInput, PmInteractOutputBuilder output) {
        if (pmInteractInput == null || pmInteractInput.getRpcAction() == null) {
            LOG.warn("The RPC action input is null");
            output.setStatus(RpcStatus.Failed)
                    .setStatusMessage("The RPC action input is null");
            return false;
        }
        HoneynodeRpcActions rpcAction = pmInteractInput.getRpcAction();
        switch (rpcAction) {
            case Get:
                if (pmInteractInput.getPmToGetClearOrDelete() != null &&
                        pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry() != null)
                    for (org.opendaylight.yang.gen.v1
                            .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.get.clear.or.delete
                            .CurrentPmEntry currentPmEntry :
                            pmInteractInput.getPmToGetClearOrDelete().getCurrentPmEntry()) {
                        if (currentPmEntry.getPmResourceInstance() == null ||
                                currentPmEntry.getPmResourceType() == null ||
                                currentPmEntry.getPmResourceTypeExtension() == null) {
                            LOG.warn(INVALID_CURRENT_PM_ENTRY_MSG);
                            output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage(INVALID_CURRENT_PM_ENTRY_MSG);
                            return false;
                        }
                    }
                break;
            case Delete:
            case Clear:
                PmToGetClearOrDelete pmToGetClearOrDelete = pmInteractInput.getPmToGetClearOrDelete();
                if (pmToGetClearOrDelete == null || pmToGetClearOrDelete.getCurrentPmEntry() == null) {
                    LOG.warn(INVALID_PM_GET_CLEAR_OR_DELETE);
                    output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(INVALID_PM_GET_CLEAR_OR_DELETE);
                    return false;
                }
                for (org.opendaylight.yang.gen.v1
                        .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.get.clear.or.delete
                        .CurrentPmEntry currentPmEntry : pmToGetClearOrDelete.getCurrentPmEntry()) {
                    if (currentPmEntry.getPmResourceInstance() == null ||
                            currentPmEntry.getPmResourceType() == null ||
                            currentPmEntry.getPmResourceTypeExtension() == null ||
                            (rpcAction == HoneynodeRpcActions.Clear &&
                            (currentPmEntry.getCurrentPm() == null || currentPmEntry.getCurrentPm().isEmpty()))) {
                        LOG.warn(INVALID_CURRENT_PM_ENTRY_MSG);
                        output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(INVALID_CURRENT_PM_ENTRY_MSG);
                        return false;
                    }
                    if (currentPmEntry.getCurrentPm() != null) {
                        for (org.opendaylight.yang.gen.v1
                                .http.honeynode.simulator.pm.handling.rev211021
                                .pm.interact.input.pm.to.get.clear.or.delete.current.pm.entry.CurrentPm currentPm :
                                currentPmEntry.getCurrentPm()) {
                            if (currentPm.getDirection() == null ||
                                    currentPm.getLocation() == null ||
                                    currentPm.getExtension() == null ||
                                    currentPm.getType() == null) {
                                LOG.warn(INVALID_CURRENT_PM_MSG);
                                output.setStatus(RpcStatus.Failed)
                                        .setStatusMessage(INVALID_CURRENT_PM_MSG);
                                return false;
                            }
                        }
                    }
                }
                break;

            case Set:
            case Create:
                PmToBeSetOrCreated pmToBeSetOrCreated = pmInteractInput.getPmToBeSetOrCreated();
                if (pmToBeSetOrCreated == null ||
                        pmToBeSetOrCreated.getCurrentPmEntry() == null ||
                        pmToBeSetOrCreated.getCurrentPmEntry().isEmpty()) {
                    LOG.warn(INVALID_PM_BE_SET_OR_CREATED);
                    output.setStatus(RpcStatus.Failed)
                            .setStatusMessage(INVALID_PM_BE_SET_OR_CREATED);
                    return false;
                }
                for (org.opendaylight.yang.gen.v1
                        .http.honeynode.simulator.pm.handling.rev211021.pm.interact.input.pm.to.be.set.or.created
                        .CurrentPmEntry currentPmEntry : pmToBeSetOrCreated.getCurrentPmEntry()) {
                    if (currentPmEntry.getCurrentPm() == null ||
                            currentPmEntry.getStartTime() == null ||
                            currentPmEntry.getPmResourceInstance() == null ||
                            currentPmEntry.getPmResourceType() == null ||
                            currentPmEntry.getPmResourceTypeExtension() == null) {
                        LOG.warn(INVALID_CURRENT_PM_ENTRY_MSG);
                        output.setStatus(RpcStatus.Failed)
                                .setStatusMessage(INVALID_CURRENT_PM_ENTRY_MSG);
                        return false;
                    }
                    for (CurrentPm currentPm : currentPmEntry.getCurrentPm()) {
                        if (currentPm.getDirection() == null ||
                                currentPm.getLocation() == null ||
                                currentPm.getExtension() == null ||
                                currentPm.getType() == null ||
                                currentPm.getMeasurement() == null) {
                            LOG.warn(INVALID_CURRENT_PM_MSG);
                            output.setStatus(RpcStatus.Failed)
                                    .setStatusMessage(INVALID_CURRENT_PM_MSG);
                            return false;
                        }
                        for (Measurement measurement : currentPm.getMeasurement()) {
                            if (measurement.getPmParameterValue() == null ||
                                    measurement.getGranularity() == null) {
                                LOG.warn(INVALID_MEASUREMENT);
                                output.setStatus(RpcStatus.Failed)
                                        .setStatusMessage(INVALID_MEASUREMENT);
                                return false;
                            }
                        }
                    }
                }
                break;

            default:
                output.setStatus(RpcStatus.Failed).setStatusMessage("Unexpected RPC-Action");
                return false;
        }
        return true;
    }
}
