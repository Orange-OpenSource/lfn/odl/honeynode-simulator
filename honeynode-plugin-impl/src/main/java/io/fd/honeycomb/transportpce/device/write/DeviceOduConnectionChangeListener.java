/*
 * Copyright (c) 2021 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;

import java.util.Collection;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.OduConnection;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import io.fd.honeycomb.transportpce.device.utils.DeviceUtil;

/**
 * @author Gilles Thouenon - Orange
 */
public final class DeviceOduConnectionChangeListener implements DataTreeChangeListener<OduConnection> {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceOduConnectionChangeListener.class);
    @Inject
    private DeviceUtil deviceUtil;

    @Override
    public void onDataTreeChanged(Collection<DataTreeModification<OduConnection>> changes) {
        LOG.info("onDataTreeChanged - OduConnection");
        for (DataTreeModification<OduConnection> change : changes) {
            final DataObjectModification<OduConnection> rootNode = change.getRootNode();
            final DataTreeIdentifier<OduConnection> rootPath = change.getRootPath();
            LOG.debug("Received OduConnection change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                rootNode.getModificationType(), rootNode.getDataType(), rootNode.getDataBefore(),
                rootNode.getDataAfter(), rootNode.getIdentifier());
            InstanceIdentifier<OduConnection> connectionId = null;
            if (rootPath.getRootIdentifier().getTargetType().getName().equals(OduConnection.class.getName())) {
                connectionId = (InstanceIdentifier<OduConnection>) InstanceIdentifier
                    .create(rootPath.getRootIdentifier().getPathArguments());
            }
            switch (rootNode.getModificationType()) {
                case SUBTREE_MODIFIED:
                case WRITE:
                    LOG.info("OduConnection is being created or modified");
                    copyConnectionOnOperationalDS(rootNode.getDataAfter(), connectionId);
                    break;
                case DELETE:
                    LOG.info("OduConnection is being deleted");
                    deviceUtil.deleteContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, connectionId);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * copy the connection in the operational datastore after a change received on
     * the config datastore.
     *
     * @param connectionAfter
     *            new configuration of the circuit-pack
     * @param id
     *            Instance Identifier of the circuit-pack
     */
    private void copyConnectionOnOperationalDS(OduConnection connectionAfter, InstanceIdentifier<OduConnection> id) {
        if (id == null) {
            LOG.warn("Error to copy connection in operational DS - connection instance id is null");
            return;
        }
        OduConnection connection = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, id);
        if (connection != null) {
            LOG.warn("Error creating connection {} - it already exists", connectionAfter.getConnectionName());
            return;
        }
        LOG.info("creating new connection {} in operational DS", connectionAfter.getConnectionName());
        deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, id, connectionAfter);
    }

}