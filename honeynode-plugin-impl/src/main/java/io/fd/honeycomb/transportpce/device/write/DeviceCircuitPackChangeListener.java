/*
 * Copyright (c) 2021 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;

import java.util.Collection;
import org.opendaylight.mdsal.binding.api.DataObjectModification;
import org.opendaylight.mdsal.binding.api.DataTreeChangeListener;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.binding.api.DataTreeModification;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacksBuilder;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import io.fd.honeycomb.transportpce.device.utils.DeviceUtil;

/**
 * @author Gilles Thouenon - Orange
 */
public final class DeviceCircuitPackChangeListener implements DataTreeChangeListener<CircuitPacks> {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceCircuitPackChangeListener.class);
    @Inject
    private DeviceUtil deviceUtil;

    @Override
    public void onDataTreeChanged(Collection<DataTreeModification<CircuitPacks>> changes) {
        LOG.info("onDataTreeChanged - CircuitPacks");
        for (DataTreeModification<CircuitPacks> change : changes) {
            final DataObjectModification<CircuitPacks> rootNode = change.getRootNode();
            final DataTreeIdentifier<CircuitPacks> rootPath = change.getRootPath();

            if (rootNode != null && rootNode.getDataBefore() != null
                && !rootNode.getDataBefore().equals(rootNode.getDataAfter())
                && isRealCircuitPackModif(rootNode)) {
                LOG.debug("Received Circuit-pack change({}):\n dataType={} \n before={} \n after={} \n identifier = {}",
                    rootNode.getModificationType(), rootNode.getDataType(), rootNode.getDataBefore(),
                    rootNode.getDataAfter(), rootNode.getIdentifier());
                InstanceIdentifier<CircuitPacks> cpId = null;
                if (rootPath.getRootIdentifier().getTargetType().getName().equals(CircuitPacks.class.getName())) {
                    cpId = (InstanceIdentifier<CircuitPacks>) InstanceIdentifier
                        .create(rootPath.getRootIdentifier().getPathArguments());
                }
                switch (rootNode.getModificationType()) {
                    case SUBTREE_MODIFIED:
                    case WRITE:
                        LOG.info("circuit-pack is being modified");
                        updateCircuitPackOnOperationalDS(rootNode.getDataBefore(), rootNode.getDataAfter(), cpId);
                        break;
                    case DELETE:
                        LOG.info("circuit-pack is being deleted");
                        break;
                    default:
                        break;
                    }
            }
        }
    }

    private boolean isRealCircuitPackModif(DataObjectModification<CircuitPacks> rootNode) {
        if (rootNode.getDataAfter() == null || rootNode.getDataBefore() == null) {
            return false;
        }
        LOG.debug("Verifying if real modified children for CircuitPacks {}",
            rootNode.getDataBefore().getCircuitPackName());
        CircuitPacks cpBefore = new CircuitPacksBuilder(rootNode.getDataBefore())
            .setPorts(null)
            .build();
        CircuitPacks cpAfter = new CircuitPacksBuilder(rootNode.getDataAfter())
            .setPorts(null)
            .build();
        return !cpAfter.equals(cpBefore);
    }

    /**
     * update the circuit-pack in the operational datastore after a change received
     * on the config datastore.
     *
     * @param cpBefore
     *            initial configuration of the circuit-pack
     * @param cpAfter
     *            new configuration of the circuit-pack
     * @param id
     *            Instance Identifier of the circuit-pack
     */
    private synchronized void updateCircuitPackOnOperationalDS(CircuitPacks cpBefore, CircuitPacks cpAfter,
        InstanceIdentifier<CircuitPacks> id) {
        if (id == null) {
            LOG.warn("Error to update circuit-pack in operational DS - circuit-pack instance id is null");
            return;
        }
        CircuitPacks oldCircuitPack = deviceUtil.readContainerFromDataStore(LogicalDatastoreType.OPERATIONAL, id);
        CircuitPacksBuilder cpBldr = new CircuitPacksBuilder(oldCircuitPack);
        if ((cpBefore.getAdministrativeState() != null && cpAfter.getAdministrativeState() != null
            && !cpBefore.getAdministrativeState().equals(cpAfter.getAdministrativeState()))
            || (cpBefore.getAdministrativeState() == null && cpAfter.getAdministrativeState() != null)) {
            LOG.info("administrative state of circuit-pack {} changed", cpBefore.getCircuitPackName());
            cpBldr.setAdministrativeState(cpAfter.getAdministrativeState())
                .setOperationalState(deviceUtil.setOperationalStateFromAdminState(cpAfter.getAdministrativeState()));
        }
        if ((cpBefore.getEquipmentState() != null && cpAfter.getEquipmentState() != null
            && !cpBefore.getEquipmentState().equals(cpAfter.getEquipmentState()))
            || (cpBefore.getEquipmentState() == null && cpAfter.getEquipmentState() != null)) {
            LOG.info("equipment state of circuit-pack {} changed", cpBefore.getCircuitPackName());
            cpBldr.setEquipmentState(cpAfter.getEquipmentState());
        }
        deviceUtil.writeContainerIntoDataStore(LogicalDatastoreType.OPERATIONAL, id, cpBldr.build());
    }
}
