/*
 * Copyright (c) 2021 Orange.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.utils;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

import org.eclipse.jdt.annotation.NonNull;
import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.ReadTransaction;
import org.opendaylight.mdsal.binding.api.WriteTransaction;
import org.opendaylight.mdsal.common.api.CommitInfo;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.common.state.types.rev191129.State;
import org.opendaylight.yang.gen.v1.http.org.openroadm.equipment.states.types.rev191129.AdminStates;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FluentFuture;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public final class DeviceUtil {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceUtil.class);
    @Inject
    @Named("device-databroker")
    private DataBroker dataBroker;

    /**
     * get container from OrgOpenroadmDevice datastore.
     *
     * @param datastore
     *            LogicalDatastoreType
     * @param path
     *            Instance Identifier of the container
     * @return container or null if read transaction issue
     */
    public <T extends DataObject> T readContainerFromDataStore(LogicalDatastoreType datastore,
            InstanceIdentifier<T> path) {
        LOG.info("reading {} from {} datastore ...", path.getTargetType().getSimpleName(), datastore);
        try (ReadTransaction readTx = this.dataBroker.newReadOnlyTransaction()) {
            Optional<T> optional = readTx.read(datastore, path).get();
            if (optional.isPresent()) {
                return optional.get();
            }
        } catch (InterruptedException e) {
            LOG.error("Failed to process ReadTransaction", e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            LOG.error("Failed to process ReadTransaction", e);
        }
        return null;
    }

    /**
     * write a container into OrgOpenroadmDevice datastore.
     *
     * @param datastore
     *            LogicalDatastoreType
     * @param path
     *            Instance Identifier of the container
     * @param container
     *            to write/update into the device datastore
     */
    public <T extends DataObject> void writeContainerIntoDataStore(LogicalDatastoreType datastore,
            InstanceIdentifier<T> path, T container) {
        LOG.info("writing container {} into {} datastore ...", path.getTargetType().getSimpleName(), datastore);
        WriteTransaction writeTx = this.dataBroker.newWriteOnlyTransaction();
        try {
            writeTx.merge(datastore, path, container);
            FluentFuture<? extends @NonNull CommitInfo> future = writeTx.commit();
            future.get();
        } catch (InterruptedException e) {
            LOG.error("Failed to process WriteTransaction", e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            LOG.error("Failed to process WriteTransaction", e);
        }
    }

    /**
     * overwrite a container into OrgOpenroadmDevice datastore.
     *
     * @param datastore
     *            LogicalDatastoreType
     * @param path
     *            Instance Identifier of the container
     * @param container
     *            to write/update into the device datastore
     */
    public <T extends DataObject> void overwriteContainerIntoDataStore(LogicalDatastoreType datastore,
            InstanceIdentifier<T> path, T container) {
        LOG.info("writing container {} into {} datastore ...", path.getTargetType().getSimpleName(), datastore);
        WriteTransaction writeTx = this.dataBroker.newWriteOnlyTransaction();
        try {
            writeTx.put(datastore, path, container);
            FluentFuture<? extends @NonNull CommitInfo> future = writeTx.commit();
            future.get();
        } catch (InterruptedException e) {
            LOG.error("Failed to process WriteTransaction", e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            LOG.error("Failed to process WriteTransaction", e);
        }
    }

    /**
     * delete a container from OrgOpenroadmDevice datastore.
     *
     * @param datastore
     *            LogicalDatastoreType
     * @param path
     *            Instance Identifier of the container
     */
    public <T extends DataObject> void deleteContainerFromDataStore(LogicalDatastoreType datastore,
            InstanceIdentifier<T> path) {
        LOG.info("deleting container {} from {} datastore ...", path.getTargetType().getSimpleName(), datastore);
        WriteTransaction writeTx = this.dataBroker.newWriteOnlyTransaction();
        try {
            writeTx.delete(datastore, path);
            FluentFuture<? extends @NonNull CommitInfo> future = writeTx.commit();
            future.get();
        } catch (InterruptedException e) {
            LOG.error("Failed to process DeleteTransaction", e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            LOG.error("Failed to process DeleteTransaction", e);
        }
    }

    /**
     * update the operational state of any device container (port, circuit-pack,
     * etc) according to the administrative-state configured on it.
     *
     * @param adminState
     *            AdminStates configured on the container
     * @return operational-state of the container for operational datastore purpose
     */
    public final State setOperationalStateFromAdminState(AdminStates adminState) {
        if (adminState == null) {
            return null;
        }
        switch (adminState.getName()) {
        case "inService":
            return State.InService;
        case "maintenance":
            return State.Degraded;
        case "outOfService":
            return State.OutOfService;
        default:
            return null;
        }
    }
}
