/*
 * Copyright (c) 2022 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.notifications;

import io.fd.honeycomb.notification.ManagedNotificationProducer;
import io.fd.honeycomb.notification.NotificationCollector;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.ChangeNotification;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.ChangeNotificationBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.change.notification.Edit;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.change.notification.EditBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.changed.by.parms.ChangedBy;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.changed.by.parms.ChangedByBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.changed.by.parms.changed.by.server.or.user.ServerBuilder;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.netconf.base._1._0.rev110601.EditOperationType;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.yang.types.rev130715.DateAndTime;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.binding.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceNotificationProducer implements ManagedNotificationProducer {
    private static final Logger LOG = LoggerFactory.getLogger(DeviceNotificationProducer.class);

    private NotificationCollector collector;

    /**
     * Send the change-notification of org-openroadm-device with the port
     * instance-id concerned by the port modification.
     *
     * @param id Instance Identifier of the port
     */
    public void sendDeviceChangeNotification(InstanceIdentifier<Ports> id) {
        ChangeNotification changeNotification = createChangeNotification(id);
        if (this.collector != null) {
            LOG.info("Sending ChangeNotification notification");
            this.collector.onNotification(changeNotification);
        } else {
            LOG.warn("collector is null - impossible to send notification");
        }
    }

    /**
     * Prepare the change-notification of org-openroadm-device with the port
     * instance-id concerned by the port modification.
     *
     * @param id Instance Identifier of the port
     */
    private ChangeNotification createChangeNotification(InstanceIdentifier<Ports> id) {
        List<Edit> editList = new ArrayList<>();
        Edit edit = new EditBuilder()
                .setOperation(EditOperationType.Merge)
                .setTarget(id)
                .build();
        editList.add(edit);
        ChangedBy changedBy = new ChangedByBuilder().setServerOrUser(new ServerBuilder().setServer(true).build())
                .build();
        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX").format(new Date());
        return new ChangeNotificationBuilder().setChangedBy(changedBy).setChangeTime(new DateAndTime(time))
                .setDatastore(ChangeNotification.Datastore.Running).setEdit(editList).build();
    }

    @Override
    public Collection<Class<? extends Notification>> getNotificationTypes() {
        return new ArrayList<>(List.of(ChangeNotification.class));
    }

    @Override
    public void close() throws Exception {
        LOG.info("Closing notification stream for DeviceNotificationProducer");
        collector.close();
    }

    @Override
    public void start(NotificationCollector collector) {
        LOG.info("Starting notification stream for DeviceNotificationProducer");
        this.collector = collector;
    }

    @Override
    public void stop() {
        // TODO Auto-generated method stub
    }
}
