/*
 * Copyright (c) 2018 Orange and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.fd.honeycomb.transportpce.device.write;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import io.fd.honeycomb.translate.util.write.BindingBrokerWriter;
import io.fd.honeycomb.translate.write.WriterFactory;
import io.fd.honeycomb.translate.write.registry.ModifiableWriterRegistryBuilder;
import javax.annotation.Nonnull;

import org.opendaylight.mdsal.binding.api.DataBroker;
import org.opendaylight.mdsal.binding.api.DataTreeIdentifier;
import org.opendaylight.mdsal.common.api.LogicalDatastoreType;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.pack.Ports;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.circuit.packs.CircuitPacks;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.interfaces.grp.Interface;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.OrgOpenroadmDevice;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.OduConnection;
import org.opendaylight.yang.gen.v1.http.org.openroadm.device.rev200529.org.openroadm.device.container.org.openroadm.device.RoadmConnections;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;

public class DeviceWriterFactory implements WriterFactory {
    private static final InstanceIdentifier<OrgOpenroadmDevice> DEVICE_CONTAINER_ID = InstanceIdentifier
        .create(OrgOpenroadmDevice.class);
    private static final InstanceIdentifier<Interface> DEVICE_INTERFACE_CONTAINER_ID = InstanceIdentifier
        .create(OrgOpenroadmDevice.class).child(Interface.class);
    private static final InstanceIdentifier<RoadmConnections> DEVICE_ROADM_CONNECTION_CONTAINER_ID = InstanceIdentifier
        .create(OrgOpenroadmDevice.class).child(RoadmConnections.class);
    private static final InstanceIdentifier<OduConnection> DEVICE_ODU_CONNECTION_CONTAINER_ID = InstanceIdentifier
        .create(OrgOpenroadmDevice.class).child(OduConnection.class);
    private static final InstanceIdentifier<CircuitPacks> DEVICE_CP_CONTAINER_ID = InstanceIdentifier
        .create(OrgOpenroadmDevice.class).child(CircuitPacks.class);
    private static final InstanceIdentifier<Ports> DEVICE_PORT_CONTAINER_ID = InstanceIdentifier
        .create(OrgOpenroadmDevice.class).child(CircuitPacks.class).child(Ports.class);

    @Inject
    @Named("device-databroker")
    private DataBroker deviceDataBroker;
    @Inject
    private DevicePortChangeListener dpcl;
    @Inject
    private DeviceCircuitPackChangeListener dcpl;
    @Inject
    private DeviceInterfaceChangeListener dicl;
    @Inject
    private DeviceRoadmConnectionChangeListener drccl;
    @Inject
    private DeviceOduConnectionChangeListener doccl;

    @Override
    public void init(@Nonnull final ModifiableWriterRegistryBuilder registry) {
        registry.wildcardedSubtreeAdd(new BindingBrokerWriter<>(DEVICE_CONTAINER_ID, deviceDataBroker));
        deviceDataBroker.registerDataTreeChangeListener(DataTreeIdentifier.create(LogicalDatastoreType.CONFIGURATION,
            DEVICE_INTERFACE_CONTAINER_ID), dicl);
        deviceDataBroker.registerDataTreeChangeListener(DataTreeIdentifier.create(LogicalDatastoreType.CONFIGURATION,
            DEVICE_ROADM_CONNECTION_CONTAINER_ID), drccl);
        deviceDataBroker.registerDataTreeChangeListener(DataTreeIdentifier.create(LogicalDatastoreType.CONFIGURATION,
            DEVICE_ODU_CONNECTION_CONTAINER_ID), doccl);
        deviceDataBroker.registerDataTreeChangeListener(DataTreeIdentifier.create(LogicalDatastoreType.CONFIGURATION,
            DEVICE_CP_CONTAINER_ID), dcpl);
        deviceDataBroker.registerDataTreeChangeListener(DataTreeIdentifier.create(LogicalDatastoreType.CONFIGURATION,
            DEVICE_PORT_CONTAINER_ID), dpcl);
    }
}
